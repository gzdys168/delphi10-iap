program IAP;

uses
  Vcl.Forms,
  UntMain in 'UntMain.pas' {frmMain},
  DYSIni in 'DYSIni.pas',
  DYSChar in 'DYSChar.pas',
  DYSCOM in 'DYSCOM.pas',
  UntCom in 'UntCom.pas',
  UntCMD in 'UntCMD.pas',
  UntConst in 'UntConst.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
