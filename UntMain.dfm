object frmMain: TfrmMain
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'IAP'#24037#20855
  ClientHeight = 321
  ClientWidth = 484
  Color = clSkyBlue
  Font.Charset = GB2312_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #26032#23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  object pnlBottom: TPanel
    Left = 0
    Top = 301
    Width = 484
    Height = 20
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object pbIAP: TProgressBar
      Left = 0
      Top = 0
      Width = 484
      Height = 20
      Align = alClient
      TabOrder = 0
    end
  end
  object pnlTop: TPanel
    Left = 0
    Top = 0
    Width = 484
    Height = 301
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object lbCOM: TLabel
      Left = 3
      Top = 11
      Width = 24
      Height = 12
      Caption = #20018#21475
    end
    object lbBTL: TLabel
      Left = 3
      Top = 41
      Width = 36
      Height = 12
      Caption = #27874#29305#29575
    end
    object lbIAP: TLabel
      Left = 3
      Top = 72
      Width = 42
      Height = 12
      Caption = 'Bin'#25991#20214
    end
    object edtIAP: TEdit
      Left = 47
      Top = 68
      Width = 375
      Height = 20
      Color = clInfoBk
      ReadOnly = True
      TabOrder = 2
    end
    object btnDown: TButton
      Left = 47
      Top = 99
      Width = 100
      Height = 25
      Caption = 'IAP'#65288#19968#38190#21319#32423#65289
      TabOrder = 4
      OnClick = btnDownClick
    end
    object cbCOM: TComboBox
      Left = 47
      Top = 7
      Width = 100
      Height = 20
      DropDownCount = 20
      ItemIndex = 0
      TabOrder = 0
      Text = 'COM1'
      OnChange = cbCOMChange
      Items.Strings = (
        'COM1'
        'COM2'
        'COM3'
        'COM4'
        'COM5'
        'COM6'
        'COM7'
        'COM8'
        'COM9'
        'COM10'
        'COM11'
        'COM12'
        'COM13'
        'COM14'
        'COM15'
        'COM16'
        'COM17'
        'COM18'
        'COM19'
        'COM20')
    end
    object cbBTL: TComboBox
      Left = 47
      Top = 37
      Width = 100
      Height = 20
      DropDownCount = 20
      ItemIndex = 9
      TabOrder = 1
      Text = '115200'
      OnChange = cbBTLChange
      Items.Strings = (
        '1200'
        '2400'
        '4800'
        '9600'
        '14400'
        '19200'
        '28800'
        '38400'
        '57600'
        '115200')
    end
    object btnIAP: TButton
      Left = 422
      Top = 66
      Width = 25
      Height = 25
      Caption = '...'
      TabOrder = 3
      OnClick = btnIAPClick
    end
    object mmInfo: TMemo
      Left = 47
      Top = 130
      Width = 400
      Height = 160
      Color = clInfoBk
      ReadOnly = True
      ScrollBars = ssBoth
      TabOrder = 5
    end
    object btnClear: TButton
      Left = 347
      Top = 99
      Width = 100
      Height = 25
      Caption = #28165#38500#21015#34920
      TabOrder = 6
      OnClick = btnClearClick
    end
  end
  object OpenDialog1: TOpenDialog
    Filter = 'bin'#25991#20214#65288'*.bin)|*.bin'
    Left = 64
    Top = 144
  end
end
