//Test TortoiseGit
unit UntMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls,
  Vcl.ComCtrls;

const
  SET_FILE = 'IAP.ini';
  SET_SECTION = 'IAP';
  COM_NO = 'ComNo';
  BAUD_RATE = 'BaudRate';
  IAP_FILE = 'IAPFile';

type
  TfrmMain = class(TForm)
    pnlBottom: TPanel;
    pnlTop: TPanel;
    lbCOM: TLabel;
    lbBTL: TLabel;
    edtIAP: TEdit;
    btnDown: TButton;
    cbCOM: TComboBox;
    cbBTL: TComboBox;
    lbIAP: TLabel;
    btnIAP: TButton;
    pbIAP: TProgressBar;
    OpenDialog1: TOpenDialog;
    mmInfo: TMemo;
    btnClear: TButton;
    procedure FormCreate(Sender: TObject);
    procedure cbCOMChange(Sender: TObject);
    procedure cbBTLChange(Sender: TObject);
    procedure btnIAPClick(Sender: TObject);
    procedure btnDownClick(Sender: TObject);
    procedure btnClearClick(Sender: TObject);
  private
    function DownBinFile(): boolean;
  public
    function StartIAPCMD(): boolean;
    function IAPDownCMD(): boolean;
    function StartAPPCMD(): boolean;
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

uses DYSIni, UntCom, DYSChar, UntCMD, UntConst;

// 清除列表---------------------------------------------------------------------
procedure TfrmMain.btnClearClick(Sender: TObject);
begin
  mmInfo.Lines.Clear;
end;

// IAP（一键升级）--------------------------------------------------------------
procedure TfrmMain.btnDownClick(Sender: TObject);
begin
  if not FileExists(edtIAP.Text) then
  begin
    edtIAP.Text := '';
    mmInfo.Lines.Add('请先选择下载文件！');
    Exit;
  end;

  try
    Update;
    Screen.Cursor := crHourGlass;
    Application.ProcessMessages;

    btnDown.Enabled := False;
    open_port(cbCOM.Text, StrtoIntDef(cbBTL.Text, 57600));

    if (iHandle > 0) then // 打开串口成功
    begin
      if mmInfo.Lines.Count > 0 then
        mmInfo.Lines.Add('');
      mmInfo.Lines.Add('打开串口成功');

      // 1、启动IAP
      if StartIAPCMD() then
        mmInfo.Lines.Add('start_iap_cmd()成功')
      else
      begin
        mmInfo.Lines.Add('start_iap_cmd()失败！');
        Exit;
      end;
      Application.ProcessMessages;

      // 2、等待下位机重启
      mmInfo.Lines.Add('等待下位机重启...');
      Sleep(1000);
      Application.ProcessMessages;

      // 3、发送IAP命令
      if IAPDownCMD() then
        mmInfo.Lines.Add('iap_down_cmd()成功')
      else
      begin
        mmInfo.Lines.Add('iap_down_cmd()失败！');
        Exit;
      end;
      Application.ProcessMessages;

      // 4、下载数据
      if not DownBinFile() then
      begin
        mmInfo.Lines.Add('DownBinFile()失败！');
        Exit;
      end;
      Application.ProcessMessages;

      // 5、启动App
      if StartAPPCMD() then
        mmInfo.Lines.Add('start_app_cmd()成功')
      else
      begin
        mmInfo.Lines.Add('start_app_cmd()失败！');
        Exit;
      end;
      Application.ProcessMessages;

      mmInfo.Lines.Add('IAP下载完成！');
    end

    else // 打开串口失败
    begin
      if mmInfo.Lines.Count > 0 then
        mmInfo.Lines.Add('');
      mmInfo.Lines.Add('打开串口失败！');
    end;
  finally
    close_port();
    btnDown.Enabled := True;
    Screen.Cursor := crDefault;
  end;
end;

// 选择文件---------------------------------------------------------------------
procedure TfrmMain.btnIAPClick(Sender: TObject);
begin
  if OpenDialog1.Execute then
  begin
    edtIAP.Text := OpenDialog1.FileName;
    iniWriteValue(AppPath + SET_FILE, SET_SECTION, IAP_FILE,
      OpenDialog1.FileName);
  end;
end;

// 修改波特率-------------------------------------------------------------------
procedure TfrmMain.cbBTLChange(Sender: TObject);
begin
  iniWriteValue(AppPath + SET_FILE, SET_SECTION, BAUD_RATE, cbBTL.Text);
end;

// 修改串口---------------------------------------------------------------------
procedure TfrmMain.cbCOMChange(Sender: TObject);
begin
  iniWriteValue(AppPath + SET_FILE, SET_SECTION, COM_NO, cbCOM.Text);
end;

// 下载文件---------------------------------------------------------------------
function TfrmMain.DownBinFile(): boolean;
var
  i, lastCNT: integer;
  size: Longint;
  FromF: file;
  NumRead: integer;
  Buf: array [1 .. 128] of AnsiChar;
begin
  Result := False;
  mmInfo.Lines.Add('开始下载...');

  try
    AssignFile(FromF, edtIAP.Text);
    Reset(FromF, 1);

    lastCNT := 0;
    size := FileSize(FromF);
    pbIAP.Position := 0;
    pbIAP.Max := size div 128;

    repeat
      System.BlockRead(FromF, Buf, SizeOf(Buf), NumRead);
      if (NumRead > 0) then
      begin
        lastCNT := NumRead;
        if not down_data_cmd(Buf, NumRead) then
        begin
          Exit;
        end;
        pbIAP.Position := pbIAP.Position + 1;
        Application.ProcessMessages;
      end;
    until (NumRead = 0);

    if (lastCNT = 128) then
    begin
      for i := 1 to 10 do
      begin
        Buf[i] := #$FF;
      end;

      if not down_data_cmd(Buf, 10) then
      begin
        Exit;
      end;
    end;

    Result := True;
  finally
    CloseFile(FromF);
  end;
end;

// 窗体初始化-------------------------------------------------------------------
procedure TfrmMain.FormCreate(Sender: TObject);
begin
  cbCOM.Text := iniReadValue(AppPath + SET_FILE, SET_SECTION, COM_NO, 'COM1');
  cbBTL.Text := iniReadValue(AppPath + SET_FILE, SET_SECTION, BAUD_RATE,
    '115200');
  edtIAP.Text := iniReadValue(AppPath + SET_FILE, SET_SECTION, IAP_FILE, '');
end;

// 发送IAP命令------------------------------------------------------------------
function TfrmMain.IAPDownCMD(): boolean;
var
  b: boolean;
  icnt: integer;
begin
  icnt := 0;
  b := iap_down_cmd();
  while (not b) and (icnt < COM_REPEAT_CNT - 1) do
  begin
    Sleep(COM_SPACE_DELAY);
    icnt := icnt + 1;
    b := iap_down_cmd();
  end;
  Result := b;
end;

// 启动App----------------------------------------------------------------------
function TfrmMain.StartAPPCMD(): boolean;
var
  b: boolean;
  icnt: integer;
begin
  icnt := 0;
  b := start_app_cmd();
  while (not b) and (icnt < COM_REPEAT_CNT - 1) do
  begin
    Sleep(COM_SPACE_DELAY);
    icnt := icnt + 1;
    b := start_app_cmd();
  end;
  Result := b;
end;

// 启动IAP----------------------------------------------------------------------
function TfrmMain.StartIAPCMD(): boolean;
var
  b: boolean;
  icnt: integer;
begin
  icnt := 0;
  b := start_iap_cmd();
  while (not b) and (icnt < COM_REPEAT_CNT - 1) do
  begin
    Sleep(COM_SPACE_DELAY);
    icnt := icnt + 1;
    b := start_iap_cmd();
  end;
  Result := b;
end;

// 显示文件---------------------------------------------------------------------
// procedure TfrmMain.ShowFile();
// var
// FromF: file;
// NumRead: Integer;
// Buf: array [1 .. 128] of AnsiChar;
//
// i: Integer;
// lncnt: Integer;
// sStr, tmp: AnsiString;
// begin
// mm.Lines.Clear;
//
// AssignFile(FromF, edtIAP.Text);
// Reset(FromF, 1);
//
// lncnt := 0;
// repeat
// System.BlockRead(FromF, Buf, SizeOf(Buf), NumRead);
//
// tmp := '';
// for i := 1 to NumRead do
// begin
// tmp := tmp + OneCToTwoC(Buf[i]) + '  ';
// if (i mod 16) = 0 then
// begin
// sStr := IntToHex(lncnt, 5);
// mm.Lines.Add(sStr + 'h  ' + Trim(tmp));
// tmp := '';
// Inc(lncnt);
// end;
// end;
//
// until (NumRead = 0);
//
// CloseFile(FromF);
// end;

// procedure TfrmMain.ShowMsg(sPrompt, sTitle: string); // 提示信息
// begin
// if sTitle <> '' then
// Application.MessageBox(PChar(sPrompt), PChar(sTitle),
// MB_OK + MB_ICONINFORMATION)
// else
// Application.MessageBox(PChar(sPrompt), PChar(Application.Title),
// MB_OK + MB_ICONINFORMATION);
// end;

end.
