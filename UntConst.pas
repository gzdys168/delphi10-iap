unit UntConst;

interface

const
  // 通讯协议
  COM_CMD_HEAD_IDX = 0; // 0
  COM_CMD_DEV_IDX = COM_CMD_HEAD_IDX + 1; // 1
  COM_CMD_DEV_SIZE = 3;
  COM_CMD_IDX = COM_CMD_DEV_IDX + COM_CMD_DEV_SIZE; // 4
  COM_CMD_LEN_IDX = COM_CMD_IDX + 1; // 5
  COM_CMD_DATA_IDX = COM_CMD_LEN_IDX + 1; // 6
  COM_CMD_MIN_SIZE = COM_CMD_LEN_IDX + 1 + 3; // 9
  COM_CMD_END_FLAG1 = #$0D;
  COM_CMD_END_FLAG2 = #$0A;

  // 通讯命令
  COM_CMD_IAP = #$99;

implementation

end.
