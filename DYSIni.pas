unit DYSIni;

interface

uses
  System.IniFiles, System.SysUtils, Vcl.Forms;

function AppPath: string;
//function GetMyPath(sDir: string): string;

function iniReadValue(sFile, sSection, sIdent: string; dValue: integer = 0)
  : integer; overload; // ���غ���

function iniReadValue(sFile, sSection, sIdent: string; dValue: string = '')
  : string; overload;

function iniReadValue(sFile, sSection, sIdent: string; dValue: boolean = False)
  : boolean; overload;

function iniReadValue(sFile, sSection, sIdent: string; dValue: Double = 0)
  : Double; overload;

procedure iniWriteValue(sFile, sSection, sIdent: string;
  dValue: integer); overload;

procedure iniWriteValue(sFile, sSection, sIdent: string;
  dValue: string); overload;

procedure iniWriteValue(sFile, sSection, sIdent: string;
  dValue: boolean); overload;

procedure iniWriteValue(sFile, sSection, sIdent: string;
  dValue: Double); overload;

implementation

function AppPath: string;
var
  sPath: string;
begin
  sPath := ExtractFilePath(Application.ExeName);
  if sPath[Length(sPath)] <> '\' then
    Result := sPath + '\'
  else
    Result := sPath;
end;

function GetMyPath(sDir: string): string;
begin
  if not DirectoryExists(sDir) then
    ForceDirectories(sDir);
  Result := sDir;
end;

function FormatFile(sFile: string): string;
begin
  Result := sFile;

  if Pos(':', sFile) = 0 then
  begin
    Result := AppPath + sFile;
  end;
end;

function iniReadValue(sFile, sSection, sIdent: string; dValue: integer)
  : integer;
var
  iniFile: TiniFile;
begin
  iniFile := TiniFile.Create(sFile);
  with iniFile do
  begin
    Result := ReadInteger(sSection, sIdent, dValue);
    Free;
  end;
  if Result = dValue then
    iniWriteValue(sFile, sSection, sIdent, dValue);
end;

function iniReadValue(sFile, sSection, sIdent: string; dValue: string): string;
var
  iniFile: TiniFile;
begin
  iniFile := TiniFile.Create(sFile);
  with iniFile do
  begin
    Result := ReadString(sSection, sIdent, dValue);
    Free;
  end;
  if (Result = dValue) then
    iniWriteValue(sFile, sSection, sIdent, dValue);
end;

function iniReadValue(sFile, sSection, sIdent: string; dValue: boolean)
  : boolean;
var
  iniFile: TiniFile;
begin
  iniFile := TiniFile.Create(sFile);
  with iniFile do
  begin
    Result := ReadBool(sSection, sIdent, dValue);
    Free;
  end;
  if Result = dValue then
    iniWriteValue(sFile, sSection, sIdent, dValue);
end;

function iniReadValue(sFile, sSection, sIdent: string; dValue: Double): Double;
var
  iniFile: TiniFile;
begin
  iniFile := TiniFile.Create(sFile);
  with iniFile do
  begin
    Result := ReadFloat(sSection, sIdent, dValue);
    Free;
  end;
  if Result = dValue then
    iniWriteValue(sFile, sSection, sIdent, dValue);
end;

procedure iniWriteValue(sFile, sSection, sIdent: string; dValue: integer);
var
  iniFile: TiniFile;
begin
  iniFile := TiniFile.Create(sFile);
  with iniFile do
  begin
    WriteInteger(sSection, sIdent, dValue);
    Free;
  end;
end;

procedure iniWriteValue(sFile, sSection, sIdent: string; dValue: string);
var
  iniFile: TiniFile;
begin
  iniFile := TiniFile.Create(sFile);
  with iniFile do
  begin
    WriteString(sSection, sIdent, dValue);
    Free;
  end;
end;

procedure iniWriteValue(sFile, sSection, sIdent: string; dValue: boolean);
var
  iniFile: TiniFile;
begin
  iniFile := TiniFile.Create(sFile);
  with iniFile do
  begin
    WriteBool(sSection, sIdent, dValue);
    Free;
  end;
end;

procedure iniWriteValue(sFile, sSection, sIdent: string; dValue: Double);
var
  iniFile: TiniFile;
begin
  iniFile := TiniFile.Create(sFile);
  with iniFile do
  begin
    WriteFloat(sSection, sIdent, dValue);
    Free;
  end;
end;

end.
