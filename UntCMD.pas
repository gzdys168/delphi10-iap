unit UntCMD;

interface

const
  COM_REPEAT_CNT = 3;
  COM_SPACE_DELAY = 100;
  COM_READ_DELAY = 100;
  COM_BUFF_SIZE = 255;

function start_iap_cmd(): boolean; // 启动IAP
function iap_down_cmd(): boolean; // 发送IAP命令
function down_data_cmd(A: array of AnsiChar; N: integer): boolean; // 下载数据
function start_app_cmd(): boolean; // 启动App

var
  rdCount: integer;
  wbuf: array [0 .. COM_BUFF_SIZE] of AnsiChar;
  rbuf: array [0 .. COM_BUFF_SIZE] of AnsiChar;

implementation

uses DYSCOM, DYSChar, UntCom, UntConst;

// 启动IAP----------------------------------------------------------------------
function start_iap_cmd(): boolean;
begin
  Result := False;

  if (iHandle = -1) then // 串口没打开
  begin
    Exit;
  end;

  wbuf[0] := #$2A; // 起始

  wbuf[1] := #$51; // 机号
  wbuf[2] := #$05;
  wbuf[3] := #$07;

  wbuf[4] := COM_CMD_IAP; // 命令

  wbuf[5] := #$00; // 数据长度

  // wbuf[6] := GET_XOR(wbuf, 0, 5); // 异或校验

  wbuf[7] := COM_CMD_END_FLAG1; // 结束符
  wbuf[8] := COM_CMD_END_FLAG2;

  // 发送
  if (SP_Write(iHandle, wbuf, 0, 9) <> 9) then
  begin
    Exit;
  end;

  // 接收
  // 无APP时："not find command\r\n"
  if (SP_Read(iHandle, rbuf, COM_CMD_MIN_SIZE, COM_READ_DELAY) <>
    COM_CMD_MIN_SIZE) then // 读取失败
  begin
    Exit;
  end;

  Result := True;
end;

// 发送IAP命令------------------------------------------------------------------
function iap_down_cmd(): boolean;
begin
  Result := False;

  if (iHandle = -1) then // 串口没打开
  begin
    Exit;
  end;

  // "iap_down"
  wbuf[0] := AnsiChar('i');
  wbuf[1] := AnsiChar('a');
  wbuf[2] := AnsiChar('p');
  wbuf[3] := AnsiChar('_');
  wbuf[4] := AnsiChar('d');
  wbuf[5] := AnsiChar('o');
  wbuf[6] := AnsiChar('w');
  wbuf[7] := AnsiChar('n');
  wbuf[8] := COM_CMD_END_FLAG1;
  wbuf[9] := COM_CMD_END_FLAG2;

  // 发送
  if (SP_Write(iHandle, wbuf, 0, 10) <> 10) then
  begin
    Exit;
  end;

  // 接收
  // "error\r\n"
  // "begin,wait data download\r\n"
  rdCount := SP_Read(iHandle, rbuf, 26, COM_READ_DELAY * 100); // 要进行Flash擦除
  if (rdCount < COM_CMD_MIN_SIZE) then // 读取失败
  begin
    Exit;
  end;

  Result := True;
end;

// 下载数据---------------------------------------------------------------------
function down_data_cmd(A: array of AnsiChar; N: integer): boolean;
var
  i: integer;
begin
  Result := False;

  if (iHandle = -1) then // 串口没打开
  begin
    Exit;
  end;

  // wbuf[0] := AnsiChar(N);
  wbuf[0] := AnsiChar(N + 2); //后续数据长度，数据带CRC检验功能

  for i := 0 to N - 1 do
  begin
    wbuf[i + 1] := A[i];
  end;

  // 数据带CRC检验功能
  Calculate_CRC(wbuf + 1, N);
  wbuf[N + 1] := crcHigh;
  wbuf[N + 2] := crcLow;

  // 发送
  // if (SP_Write(iHandle, wbuf, 0, N + 1) <> N + 1) then
  if (SP_Write(iHandle, wbuf, 0, N + 3) <> N + 3) then // 数据带CRC检验功能
  begin
    Exit;
  end;

  // 接收
  // "."，"download over\r\n"
  rdCount := SP_Read(iHandle, rbuf, 1, COM_READ_DELAY);
  if (rdCount = 0) then // 读取失败
  begin
    Exit;
  end;

  Result := True;
end;

// 启动App----------------------------------------------------------------------
function start_app_cmd(): boolean;
begin
  Result := False;

  if (iHandle = -1) then // 串口没打开
  begin
    Exit;
  end;

  // "iap_jump_app"
  wbuf[0] := AnsiChar('i');
  wbuf[1] := AnsiChar('a');
  wbuf[2] := AnsiChar('p');
  wbuf[3] := AnsiChar('_');
  wbuf[4] := AnsiChar('j');
  wbuf[5] := AnsiChar('u');
  wbuf[6] := AnsiChar('m');
  wbuf[7] := AnsiChar('p');
  wbuf[8] := AnsiChar('_');
  wbuf[9] := AnsiChar('a');
  wbuf[10] := AnsiChar('p');
  wbuf[11] := AnsiChar('p');
  wbuf[12] := COM_CMD_END_FLAG1;
  wbuf[13] := COM_CMD_END_FLAG2;

  // 发送
  if (SP_Write(iHandle, wbuf, 0, 14) <> 14) then
  begin
    Exit;
  end;

  // 接收
  // "ok\r\n"
  // "program in flash is error\r\n"
  rdCount := SP_Read(iHandle, rbuf, 27, COM_READ_DELAY);
  if (rdCount > COM_CMD_MIN_SIZE) then // 读取失败
  begin
    Exit;
  end;

  Result := True;
end;

end.
