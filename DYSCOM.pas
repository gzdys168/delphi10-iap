unit DYSCOM;

interface

uses
  Winapi.Windows;

const
  IC_DELAY_PARITY = 9; // 通讯参数 与 C 对应

  IC_PARITY_NO = 0;
  IC_PARITY_ODD = 1;
  IC_PARITY_EVEN = 2;
  IC_PARITY_MARK = 3;
  IC_PARITY_SPACE = 4;

type
  TMiscType = (IC_OP_SPEED, IC_OP_PARITY, IC_OP_STOP_BIT);

function SP_Open(COMNO: String; speed: integer): integer;
function SP_Close(iHandle: integer): boolean;

function SP_Read(iHandle: integer; Buf: Pansichar; size: LongInt;
  Timeout: LongInt): LongInt;
function SP_Write(iHandle: integer; Buf: Pansichar;
  iStart, size: LongInt): dWord;

procedure SP_Purge(iHandle: integer);

implementation

function af_Time: Int64;
var
  t: Int64;
  st: SystemTime;
begin
  GetLocalTime(st);
  SystemTimeToFileTime(st, FileTime(t));
  Result := t div 10000;
end;

function SP_Set_Misc(iHandle: integer; speed: integer): boolean;
var
  DCB: TDCB;
begin
  Result := False;

  if not GetCommState(iHandle, DCB) then
    Exit;

  DCB.ByteSize := 8;
  DCB.BaudRate := speed;
  DCB.Parity := 0; // Value; 不要数据检验位
  DCB.StopBits := 0; // Value; 数据停止位

  if not SetCommState(iHandle, DCB) then
    Exit;

  Result := True;
end;

function SP_Open(COMNO: String; speed: integer): integer;
var
  RC: dWord;
  iHandle: integer;
begin
  Result := -1;

  iHandle := CreateFile(Pchar(COMNO), Generic_Read or Generic_Write, 0, nil,
    Open_Existing, 0, 0);

  if iHandle = -1 then
    Exit; // INVALID_HANDLE_VALUE

  ClearCommError(iHandle, RC, NIL);
  ClearCommBreak(iHandle);
  SetupComm(iHandle, 4096, 4096);
  SetCommMask(iHandle, EV_ERR or EV_TXEMPTY);
  EscapeCommFunction(iHandle, SETRTS);

  SP_Purge(iHandle);

  if not SP_Set_Misc(iHandle, speed) then
    Exit;

  Result := iHandle;
end;

function SP_Close(iHandle: integer): boolean;
begin
  Result := CloseHandle(iHandle);
end;

function SP_Read(iHandle: integer; Buf: Pansichar; size: LongInt;
  Timeout: LongInt): LongInt;
var
  t: Int64;
  rb, cb: dWord;
  ct: COMMTIMEOUTS;
begin
  Result := 0;

  FillChar(ct, SizeOf(COMMTIMEOUTS), 0);

  if Timeout > 0 then
    ct.ReadTotalTimeoutConstant := Timeout
  else if Timeout = 0 then
    ct.ReadTotalTimeoutConstant := MaxDWord
  else
  begin;
  end;

  EscapeCommFunction(iHandle, SETRTS);

  if not SetCommTimeOuts(iHandle, ct) then
    Exit;

  t := af_Time + Timeout;

  cb := 0;
  repeat
    if not ReadFile(iHandle, Buf[cb], size - cb, rb, NIL) then
      Exit;
    cb := cb + rb;
  until ((cb >= size) or (af_Time >= t));

  Result := cb;
end;

function SP_Write(iHandle: integer; Buf: Pansichar;
  iStart, size: LongInt): dWord;
var
  RC, WC: dWord;
begin
  Result := 0;
  SP_Purge(iHandle); // 2016-08-10

  if not EscapeCommFunction(iHandle, CLRRTS) then
    Exit;

  if not WriteFile(iHandle, Buf[iStart], size, WC, NIL) then
    Exit;

  SleepEx(IC_DELAY_PARITY, False);

  repeat
    WaitCommEvent(iHandle, RC, NIL);
  until (0 <> (RC and EV_TXEMPTY));

  RC := 0;

  if not EscapeCommFunction(iHandle, SETRTS) then
    Exit;

  Result := WC;
end;

procedure SP_Purge(iHandle: integer);
begin
  PurgeComm(iHandle, PURGE_TXABORT or PURGE_RXABORT or PURGE_TXCLEAR or
    PURGE_RXCLEAR);
end;

end.
