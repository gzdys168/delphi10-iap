unit UntCom;

interface

procedure open_port(sCom: string; iRate: integer); // 打开串口
procedure close_port(); // 关闭串口

var
  iHandle: integer = -1;

implementation

uses DYSCOM;

// 打开串口---------------------------------------------------------------------
procedure open_port(sCom: string; iRate: integer);
begin
  if (iHandle = -1) then
  begin
    iHandle := SP_Open(PChar(sCom), iRate);
  end;
end;

// 关闭串口---------------------------------------------------------------------
procedure close_port();
begin
  if (iHandle <> -1) then
  begin
    SP_Close(iHandle);
    iHandle := -1;
  end;
end;

end.
